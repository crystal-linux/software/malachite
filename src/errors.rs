use thiserror::Error;

pub type AppResult<T> = Result<T, AppError>;

#[derive(Error, Debug)]
pub enum AppError {
    #[error("Malachite should not be run as root")]
    RunAsRoot,

    #[error(transparent)]
    Config(#[from] ConfigError),

    #[error(transparent)]
    Glob(#[from] glob::PatternError),

    #[error(transparent)]
    Git(#[from] GitError),

    #[error(transparent)]
    LibArchive(#[from] compress_tools::Error),

    #[error(transparent)]
    Gpg(#[from] gpgme::Error),

    #[error(transparent)]
    Utf8(#[from] std::string::FromUtf8Error),

    #[error(transparent)]
    Podman(#[from] podman_api::Error),

    #[error(transparent)]
    Io(#[from] IoError),

    #[error(transparent)]
    Liquid(#[from] liquid::Error),
}

#[derive(Error, Debug)]
pub enum IoError {
    #[error("IO Error: {0}")]
    Io(#[from] std::io::Error),

    #[error(transparent)]
    FsExtra(#[from] fs_extra::error::Error),
}

#[derive(Error, Debug)]
pub enum GitError {
    #[error("Invalid Git repository: {0}")]
    InvalidRepository(String),

    #[error("Unable to merge: {0}")]
    MergeError(String),
}

#[derive(Error, Debug)]
pub enum ConfigError {
    #[error("Failed to deserialize config: {0}")]
    Deserialize(#[from] toml::de::Error),

    #[error("Failed to serialize config: {0}")]
    Serialize(#[from] toml::ser::Error),

    #[error("Failed to expand repositories: {0}")]
    Expand(String),

    #[error("Failed to initialize config: {0}")]
    Init(String),
}

impl From<std::io::Error> for AppError {
    fn from(e: std::io::Error) -> Self {
        AppError::Io(IoError::Io(e))
    }
}

impl From<fs_extra::error::Error> for AppError {
    fn from(e: fs_extra::error::Error) -> Self {
        AppError::Io(IoError::FsExtra(e))
    }
}

impl From<toml::de::Error> for AppError {
    fn from(e: toml::de::Error) -> Self {
        AppError::Config(ConfigError::Deserialize(e))
    }
}

impl From<toml::ser::Error> for AppError {
    fn from(e: toml::ser::Error) -> Self {
        AppError::Config(ConfigError::Serialize(e))
    }
}
