use clap::{ArgAction, Parser, Subcommand};

use crate::fl;

static VERSION: &str = concat!(
    env!("CARGO_PKG_VERSION"),
    " (",
    env!("MALACHITE_CODENAME"),
    ")"
);

#[derive(Debug, Clone, Parser)]
#[command(bin_name = "mlc", name = "Malachite", version = VERSION, about = fl!("about"), infer_subcommands = true)]
pub struct Args {
    #[command(subcommand)]
    pub subcommand: Operation,

    #[arg(long, short, action = ArgAction::Count, global = true, help = fl!("help-verbose"))]
    pub verbose: u8,

    #[arg(long, short = 'x', action = ArgAction::Append, global = true, help = fl!("help-exclude"))]
    pub exclude: Vec<String>,
}

#[derive(Debug, Clone, Subcommand)]
pub enum Operation {
    #[command(bin_name = "mlc", name = "init", short_flag = 'I', about = fl!("help-init"))]
    Init,

    #[command(bin_name = "mlc", name = "pull", short_flag = 'P', about = fl!("help-pull"))]
    Pull {
        #[arg(long, short, action = ArgAction::SetTrue, help = fl!("help-pull-rebuild"))]
        rebuild: bool,

        #[arg(long, short, help = fl!("help-pull-concurrent"))]
        concurrent: Option<u8>,
    },

    #[command(bin_name = "mlc", name = "build", short_flag = 'B', about = fl!("help-build"))]
    Build {
        #[arg(required = true, help = fl!("help-build-packages"))]
        packages: Vec<String>,

        #[arg(long, short, help = fl!("help-build-concurrent"))]
        concurrent: Option<u8>,
    },

    #[command(bin_name = "mlc", name = "clean", short_flag = 'C', about = fl!("help-clean"))]
    Clean {
        #[arg(long, short, help = fl!("help-clean-prune"))]
        prune: Option<u8>,
    },

    #[command(bin_name = "mlc", name = "info", short_flag = 'i', about = fl!("help-info"))]
    Info,

    #[command(bin_name = "mlc", name = "generate", short_flag = 'G', about = fl!("help-generate"))]
    Generate,
}

#[derive(Default, Clone, Debug)]
pub struct GlobalArgs {
    pub verbosity: u8,
    pub exclude: Vec<String>,
}

impl GlobalArgs {
    pub fn new() -> Self {
        let args: Args = Args::parse();
        Self {
            verbosity: args.verbose,
            exclude: args.exclude,
        }
    }
}
