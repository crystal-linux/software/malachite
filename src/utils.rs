use std::path::PathBuf;
use std::process::{Child, Command, ExitStatus};

use crate::errors::{AppError, AppResult};

pub fn uid() -> i32 {
    (unsafe { libc::geteuid() } as i32)
}

#[derive(Debug, Clone)]
pub struct ShellCommand {
    pub command: String,
    pub args: Vec<String>,
    pub cwd: Option<PathBuf>,
}

impl ShellCommand {
    pub fn repo_add() -> Self {
        Self::new("repo-add")
    }

    pub fn git() -> Self {
        Self::new("git")
    }

    pub fn makepkg() -> Self {
        Self::new("makepkg")
    }

    pub fn new<S: ToString>(command: S) -> Self {
        Self {
            command: command.to_string(),
            args: vec![],
            cwd: None,
        }
    }

    pub fn arg<S: ToString>(mut self, arg: S) -> Self {
        self.args.push(arg.to_string());
        self
    }

    pub fn args<S, V>(mut self, args: V) -> Self
    where
        S: ToString,
        V: IntoIterator<Item = S>,
    {
        self.args.extend(args.into_iter().map(|s| s.to_string()));
        self
    }

    pub fn cwd<P: Into<PathBuf>>(mut self, cwd: P) -> Self {
        self.cwd = Some(cwd.into());
        self
    }

    #[allow(dead_code)]
    pub fn wait(&self) -> AppResult<ExitStatus> {
        let mut child = self.spawn()?;

        child.wait().map_err(AppError::from)
    }

    #[allow(dead_code)]
    pub fn wait_with_output(&self) -> AppResult<std::process::Output> {
        let child = self.spawn()?;

        child.wait_with_output().map_err(AppError::from)
    }

    pub fn silent(&self) -> AppResult<()> {
        let _ = self.output()?;

        Ok(())
    }

    pub fn output(&self) -> AppResult<std::process::Output> {
        let mut command = Command::new(&self.command);
        command.args(&self.args);

        if let Some(cwd) = &self.cwd {
            command.current_dir(cwd);
        }

        let output = command.output()?;

        Ok(output)
    }

    pub fn spawn(&self) -> AppResult<Child> {
        let mut command = Command::new(&self.command);
        command.args(&self.args);

        if let Some(cwd) = &self.cwd {
            command.current_dir(cwd);
        }

        let child = command.spawn()?;

        Ok(child)
    }
}
