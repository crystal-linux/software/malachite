use std::path::PathBuf;

use crate::builders::{GitCheckoutBuilder, GitCloneBuilder, GitPullBuilder};
use crate::errors::AppResult;
use crate::utils::ShellCommand;

pub struct GitRepository {
    pub path: PathBuf,
}

impl GitRepository {
    pub fn new(path: PathBuf) -> Self {
        Self { path }
    }

    pub fn clone_repo(path: PathBuf, url: &str, branch: Option<String>) -> AppResult<()> {
        let mut builder = GitCloneBuilder::new(url, path);

        if let Some(branch) = branch {
            builder = builder.branch(branch);
        }

        builder.clone()?.silent()?;

        Ok(())
    }

    pub fn pull(&self) -> AppResult<()> {
        let builder = GitPullBuilder::new(self.path.clone());

        builder.pull()?.silent()?;

        Ok(())
    }

    pub fn update_origin(&self, url: &str) -> AppResult<()> {
        ShellCommand::git()
            .cwd(self.path.clone())
            .args(["remote", "set-url", "origin"])
            .arg(url)
            .silent()?;

        Ok(())
    }

    pub fn hash(&self) -> AppResult<String> {
        let output = ShellCommand::git()
            .cwd(self.path.clone())
            .args(["rev-parse", "--short", "HEAD"])
            .output()?;

        Ok(String::from_utf8(output.stdout)?.trim().to_string())
    }

    pub fn origin(&self) -> AppResult<String> {
        let output = ShellCommand::git()
            .cwd(self.path.clone())
            .args(["remote", "get-url", "origin"])
            .output()?;

        Ok(String::from_utf8(output.stdout)?.trim().to_string())
    }

    pub fn current_branch(&self) -> AppResult<String> {
        let output = ShellCommand::git()
            .cwd(self.path.clone())
            .args(["rev-parse", "--abbrev-ref", "HEAD"])
            .output()?;

        Ok(String::from_utf8(output.stdout)?.trim().to_string())
    }

    pub fn default_branch(&self) -> AppResult<String> {
        let output = ShellCommand::git()
            .cwd(self.path.clone())
            .args(["symbolic-ref", "refs/remotes/origin/HEAD", "--short"])
            .output()?;

        let stdout = String::from_utf8(output.stdout)?.trim().to_string();

        let (_, branch) = stdout.split_once('/').unwrap();

        Ok(branch.to_string())
    }

    pub fn checkout(&self, branch: &str) -> AppResult<()> {
        GitCheckoutBuilder::new(self.path.clone(), branch)
            .checkout()?
            .silent()?;

        Ok(())
    }
}
