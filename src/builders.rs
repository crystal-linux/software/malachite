use crate::errors::AppResult;
use crate::utils::ShellCommand;
use std::path::PathBuf;

#[derive(Debug, Clone)]
pub struct GitCloneBuilder {
    pub url: String,
    pub branch: Option<String>,
    pub path: PathBuf,
}

impl GitCloneBuilder {
    pub fn new<S, P>(url: S, path: P) -> Self
    where
        S: ToString,
        P: Into<PathBuf>,
    {
        Self {
            url: url.to_string(),
            branch: None,
            path: path.into(),
        }
    }

    pub fn branch(mut self, branch: String) -> Self {
        self.branch = Some(branch);
        self
    }

    pub fn clone(&self) -> AppResult<ShellCommand> {
        let mut args = vec![
            "clone".to_string(),
            self.url.clone(),
            self.path.to_string_lossy().to_string(),
        ];

        if let Some(branch) = &self.branch {
            args.push("--branch".to_string());
            args.push(branch.clone());
        }

        let command = ShellCommand::git().args(args);

        Ok(command)
    }
}

#[derive(Debug, Clone)]
pub struct GitPullBuilder {
    pub path: PathBuf,
}

impl GitPullBuilder {
    pub fn new<P: Into<PathBuf>>(path: P) -> Self {
        Self { path: path.into() }
    }

    pub fn pull(&self) -> AppResult<ShellCommand> {
        let command = ShellCommand::git().cwd(self.path.clone()).args(["pull"]);

        Ok(command)
    }
}

#[derive(Debug, Clone)]
pub struct GitCheckoutBuilder {
    pub path: PathBuf,
    pub branch: String,
}

impl GitCheckoutBuilder {
    pub fn new<S, P>(path: P, branch: S) -> Self
    where
        S: ToString,
        P: Into<PathBuf>,
    {
        Self {
            path: path.into(),
            branch: branch.to_string(),
        }
    }

    pub fn checkout(&self) -> AppResult<ShellCommand> {
        let command = ShellCommand::git()
            .cwd(self.path.clone())
            .arg("checkout")
            .arg(self.branch.clone());

        Ok(command)
    }
}

#[derive(Debug, Clone)]
pub struct GitStatusBuilder {
    pub path: PathBuf,
}

impl GitStatusBuilder {
    pub fn new<P: Into<PathBuf>>(path: P) -> Self {
        Self { path: path.into() }
    }

    pub fn status(&self) -> AppResult<ShellCommand> {
        let command = ShellCommand::git().cwd(self.path.clone()).arg("status");

        Ok(command)
    }
}

#[derive(Debug, Clone)]
pub struct GitInitBuilder {
    pub path: PathBuf,
}

impl GitInitBuilder {
    pub fn new<P: Into<PathBuf>>(path: P) -> Self {
        Self { path: path.into() }
    }

    pub fn init(&self) -> AppResult<ShellCommand> {
        let command = ShellCommand::git().cwd(self.path.clone()).arg("init");

        Ok(command)
    }
}

#[derive(Debug, Clone)]
pub struct GitAddBuilder {
    pub path: PathBuf,
    pub refspecs: Vec<String>,
}

impl GitAddBuilder {
    pub fn new(path: PathBuf) -> Self {
        Self {
            path,
            refspecs: vec![],
        }
    }

    #[allow(dead_code)]
    pub fn refspec<S: ToString>(mut self, refspec: S) -> Self {
        self.refspecs.push(refspec.to_string());
        self
    }

    pub fn refspecs<S, V>(mut self, refspecs: V) -> Self
    where
        S: ToString,
        V: IntoIterator<Item = S>,
    {
        self.refspecs
            .extend(refspecs.into_iter().map(|s| s.to_string()));
        self
    }

    pub fn add(&self) -> AppResult<ShellCommand> {
        let command = ShellCommand::git()
            .cwd(self.path.clone())
            .arg("add")
            .args(self.refspecs.clone());

        Ok(command)
    }
}

#[derive(Debug, Clone)]
pub struct GitFetchBuilder {
    pub path: PathBuf,
}

impl GitFetchBuilder {
    pub fn new<P: Into<PathBuf>>(path: P) -> Self {
        Self { path: path.into() }
    }

    pub fn fetch(&self) -> AppResult<ShellCommand> {
        let command = ShellCommand::git().cwd(self.path.clone()).arg("fetch");

        Ok(command)
    }
}
