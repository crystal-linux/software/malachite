# src/args.rs

about = A scriptable and declarative Pacman repository management tool

help-verbose = Set amount of logging output
help-exclude = Exclude given repositories from the operation

help-init = Initializes empty Malachite repository in current directory

help-pull = Clone and/or update Git repositories
help-pull-rebuild = Rebuild packages if new commits are found
help-pull-concurrent = Number of concurrent rebuilds

help-build = Build given package(s)
help-build-packages = Package(s) to build
help-build-concurrent = Number of concurrent builds

help-clean = Remove Git repositories no longer present in the configuration
help-clean-prune = Keep X latest versions of each package, removing any that are older

help-info = Display information about the current Malachite repository

help-generate = Generate Pacman repository from built packages