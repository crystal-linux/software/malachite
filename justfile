#!/usr/bin/env just --justfile

release:
    cargo build --release

lint:
    cargo clippy --all-targets --all-features -- -D warnings
    cargo fmt -- --check

test:
    if [ -d example ]; then rm -rf example; fi
    mkdir example

    pushd example && cargo run -- init && cargo run -- pull && popd
